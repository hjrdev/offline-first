<h1 align="center">
  <img src="https://i.ibb.co/s2Nz7Vv/offline-first.png" alt="imagem na cor roxa, escrito Offline first react native, e com o símbolo do wifi riscado." border="0">
<br>
<br>
<h2 align="center">Offline First</h2>

</h1>


> Status do Projeto: :ok_hand: 👌 👌 Finalizado 🔚 🔚 🔚


## MOBILE
- ⚛️ **React Native** — A lib that provides a way to create native apps for Android and iOS

-   [React Native](https://facebook.github.io/react-native/) :sparkling_heart:
-   [TypeScript](https://www.typescriptlang.org/)
-   [Realm](https://realm.io/)
-   [Styled Components](https://www.styled-components.com/)
-   [Axios](https://github.com/axios/axios)
-   [React Native Icons](https://github.com/oblador/react-native-vector-icons)
-   [React Navigation v5](https://reactnavigation.org/)
-   [Realm](https://www.mongodb.com/realm/mobile/database)



> Note: Sorry but unfortunately, it was only possible to test on Android.


## Demostration
<br>
  <img src="https://i.ibb.co/WGPtBkM/rnrealm.png" alt="Imagem espelhada de celular, para demonstrar os repositórios do github" border="0">
<br>


## License
This project is licensed under the MIT License - see the [LICENSE](https://opensource.org/licenses/MIT) page for details.


## Author
:guitar: JR Dev :guitar:
<br />
<img src="https://avatars1.githubusercontent.com/u/6500430?s=460&u=42d7e22fa1c77b061505fe1cfc3fcaa3e2a4d1e5&v=4" width="80" alt="linkedin.com/in/junior-dev">
<br />

LinkedIn: https://www.linkedin.com/in/junior-dev <br />
Github: https://github.com/humbertoromanojr <br />
Bitbucket: https://bitbucket.org/dashboard/overview <br />
✉️ astronomi@gmail.com <br />
<br />

Made with :heart: and lots of animation by :guitar: JR Dev :guitar:
