import LinearGradient from 'react-native-linear-gradient';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import styled from 'styled-components/native';

export default {
  Container: styled(LinearGradient).attrs({
    colors: ['#42a5f5', '#0077c2'],
    start: { x: 0, y: 0 },
    end: { x: 1, y: 1 },
  })`
    flex: 1;
    padding-top: ${ 20 + getStatusBarHeight(true)}px;
  `,

  Title: styled.Text`
    color: #ffffff;
    font-size: 32px;
    font-weight: bold;
    padding: 0 20px;
  `,

  Form: styled.View`
    flex-direction: row;
    margin-top: 20px;
    padding: 0 20px;
  `,

  Input: styled.TextInput.attrs({
    placeholderTextColor: '#999999'
  })`
    flex: 1;
    padding: 12px 15px;
    border-radius: 4px;
    font-size: 18px;
    color: #333333;
    background: #ffffff;

    border: 4px solid ${props => (props.error ? '#7f0000' : '#fff')}
  `,

  Submit: styled.TouchableOpacity`
    background: #6bd4c1;
    margin-left: 10px;
    justify-content: center;
    border-radius: 4px;
    padding: 0 14px;
  `,

  List: styled.FlatList.attrs({
    contentContainerStyle: { paddingHorizontal: 20 },
    showsVerticalScrollIndicator: false,
  })`
    margin-top: 20px;
  `,
};
