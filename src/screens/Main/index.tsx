import React, { useEffect, useState } from 'react';
import { Keyboard } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Repository from '../../components/Repository';
import api from '../../services/api';
import getRealm from '../../services/realm';
import Styles from './styles';

interface IRepository {
  id: number;
  name: string;
  fullName: string;
  description: string;
  stars: number;
  forks: number;
}

export default function Main() {
  const [input, setInput] = useState('');
  const [error, setError] = useState(false);
  const [repositories, setRepositories] = useState([]);

  useState(() => {
    async function loadRepositories() {
      const realm = await getRealm();

      const data = realm.objects('Repository').sorted('stars', true);

      setRepositories(data);
    }

    loadRepositories();
  },[]);

  async function saveRepository(repository: IRepository): Promise<void> {
    const data = {
      id: repository.id,
      name: repository.name,
      fullName: repository.full_name,
      description: repository.description,
      stars: repository.stargazers_count,
      forks: repository.forks_count,
    };

    const realm = await getRealm();

    realm.write(() => {
      realm.create('Repository', data, 'modified');
    });

    return data;
  }

  async function handleAddRepository() {
    try {
      const response = await api.get(`/repos/${input}`);

      await saveRepository(response.data);
      console.log(JSON.stringify(response.data, null, 4))

      setInput('');
      setError(false);
      Keyboard.dismiss();
    } catch (err) {
      console.warn(err);
      setError(true);
    }
  }

  async function handleRefreshRepository(repository: IRepository) {
    const response = await api.get(`/repos/${repository.fullName}`);

    const data = await saveRepository(response.data);
    console.log(JSON.stringify(data, null, 4));

    setRepositories(repositories.map(repo => repo.id === data.id ? data : repo))
  }

  return (
    <Styles.Container>
      <Styles.Title>Repositórios</Styles.Title>
      <Styles.Form>
        <Styles.Input
          autoCapitalize="none"
          autoCorrect={false}
          placeholder="Procurar repositórios..."
          value={input}
          onChangeText={setInput}
          error={error}
        />
        <Styles.Submit onPress={handleAddRepository}>
          <Icon name="add" size={22} color="#ffffff" />
        </Styles.Submit>
      </Styles.Form>
      <Styles.List
        data={repositories}
        keyboardShouldPersistTaps="handle"
        keyExtractor={ item => String(item.id) }
        renderItem={({ item }) => (
          <Repository data={item} onRefresh={() => handleRefreshRepository(item)}/>
        )}
      />
    </Styles.Container>
  );
};
