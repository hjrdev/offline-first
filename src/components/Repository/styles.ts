import styled from 'styled-components/native';

export default {
  Container: styled.View`
    padding: 10px;
    border-radius: 4px;
    background: #ffffff;
    margin-top: 10px;
  `,

  Name: styled.Text`
    color: #333333;
    font-size: 18px;
    margin-bottom: 10px;
    font-weight: bold;
  `,

  Description: styled.Text.attrs({
    numberOfLines: 2,
  })`
    color: #666666;
    line-height: 20px;
  `,

  Stats: styled.View`
    margin-top: 10px;
    flex-direction: row;
  `,
  Stat: styled.View`
    flex-direction: row;
    margin-right: 20px;
    align-items: center;
  `,
  StatCount: styled.Text`
  color: #666666;
    margin-left: 5px;
  `,

  Refresh: styled.TouchableOpacity`
    margin-top: 20px;
    flex-direction: row;
    justify-content: flex-end;
  `,
  RefreshText: styled.Text`
    font-size: 14px;
    font-weight: bold;
    text-transform: uppercase;
    color: #7159c1;
    padding-right: 10px;
  `,
}
